@extends('admin.layout')

@section('content')
    <div class="row no-gutters">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet" id="kt_portlet_filters_form">
                <div class="kt-portlet__head kt-ribbon kt-ribbon--success">
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="javascript:;" data-ktportlet-tool="toggle" id="searchOptionsToggleBtn"
                               class="btn btn-sm btn-icon btn-outline-brand btn-icon-md"><i
                                        class="la la-angle-down"></i></a>
                            <a href="javascript:;" data-ktportlet-tool="reload"
                               class="btn btn-sm btn-icon btn-outline-success btn-icon-md"><i
                                        class="la la-refresh"></i></a>
                            <a href="javascript:;" data-ktportlet-tool="remove"
                               class="btn btn-sm btn-icon btn-outline-danger btn-icon-md"><i class="la la-close"></i></a>
                        </div>
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>

    <div class="kt-portlet">
        <div class="kt-portlet__body">

            <!--begin::Section-->
            <div class="kt-section">
                <div class="kt-section__content">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead class="thead-light">
                            <tr>
                                <th class="text-center font-weight-bold">ID</th>
                                <th class="text-center font-weight-bold">Sku</th>
                                <th class="text-center font-weight-bold">Наименование</th>
                                <th class="text-center font-weight-bold">Редовна цена</th>
                                <th class="text-center font-weight-bold">Промо Цена</th>
                                <th class="text-center font-weight-bold">Цвят</th>
                                <th class="text-center font-weight-bold">Размер</th>
                                <th class="text-center font-weight-bold">Описание</th>
                                <th class="text-center font-weight-bold">Създаден на</th>
                                <th class="text-center font-weight-bold">Действия</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($products as $product)
                                <tr>
                                    <td class="text-center" style="width: 1% !important;">{{ $product->id }}</td>
                                    <td>
                                        {{ $product->sku }}
                                    </td>
                                    <td>
                                        {{ $product->title }}
                                    </td>
                                    <td>
                                        {{ $product->price }} лв.
                                    </td>
                                    <td>
                                        {{ $product->promo_price }} лв.
                                    </td>
                                    <td>
                                        {{ $product->color }}
                                    </td>
                                    <td class="text-center">
                                        размер
                                    </td>
                                    <td>
                                        {{ $product->description }}
                                    </td>
                                    <td class="text-center">{{ $product->created_at }}</td>
                                    <td class="text-center">
                                        <a href="" role="button" class="btn btn-sm btn-outline-brand btn-icon"><i
                                            class="flaticon-edit"></i></a>
                                        <a href="" role="button" class="btn btn-sm btn-outline-brand btn-icon" target="_blank"><i
                                            class="flaticon-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach

                                <tr>
                                    <td colspan="42" class="text-center text-danger">Няма резултати</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">

                    </div>
                </div>
            </div>

            <!--end::Section-->
        </div>

        <!--end::Form-->
    </div>
@endsection
