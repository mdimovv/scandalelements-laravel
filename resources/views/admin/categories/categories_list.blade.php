@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="kt-portlet kt-portlet--tab">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                </span>
                    <h3 class="kt-portlet__head-title">
                        Категории
                    </h3>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="dd">
                    <ol class="dd-list">
                    @foreach($categoriesTree as $category)
                            <li class="dd-item dd3-item" data-id="{{ $category['id'] }}" data-weight="{{ $category['weight'] }}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <div class="flex-with-spaced">
                                        {{ $category['name'] }} [{{ $category['id'] }}]
                                        <div>
                                            <a href="{{ route('categories.edit', ['id' => $category['id']]) }}" role="button" class="btn btn-sm btn-outline-brand btn-icon extra-small-btn"><i
                                                    class="flaticon-edit"></i></a>
                                        </div>

                                    </div>
                                </div>
                                @if(isset($category['children']) && count($category['children']))
                                    @include('admin.categories._sub_categories', [
                                       'children' => $category['children']
                                   ])
                                @endif
                            </li>
                    @endforeach
                    </ol>
                </div>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
</div>
@endsection

@section('page_js')
@parent
<script src="{{ asset('js/jquery-nestable.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(window).ready(function(e) {
        $('.dd').nestable({ group: 1, maxDepth: 5 });
        $('.dd').on('change', function() {
            var serializedCategories = $('.dd').nestable('serialize');
            $.ajax({
                type: "POST",
                url: '{{ route('admin.categories.save') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    'category_tree': serializedCategories
                }
            });
        });
    });
</script>
@endsection

@section('page_css')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nestable.css') }}">
    <style>
        .flex-with-spaced {
            display: flex;
            justify-content: space-between;
        }
        .extra-small-btn {
            height: 20px !important;
        }
    </style>
@show