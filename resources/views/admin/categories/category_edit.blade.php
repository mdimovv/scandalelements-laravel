@extends('admin.layout')

@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    {{ $category->id ? 'Редакция': 'Създаване' }} на категория {{ $category->id ?  $category->name : '' }}
                </h3>
            </div>
        </div>
        <form method="post" action="{{ $category->id ? route('categories.edit', ['id' => $category]) : route('categories.edit') }}" id="update-product-model-form" class="kt-form kt-form--label-right" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('POST') }}

            <div class="kt-portlet__body">
                <!--begin::Section-->
                <div class="kt-section">
                    <div class="kt-section__content">
                        @include('admin.components.flashes')

                        @php $translationsFieldsConfig = [
                            'name' => [
                               'object' => $category,
                               'label' => 'Име на категорията'
                            ],
                            'slug' => [
                                'object' => $category,
                                'label' => 'Slug'
                            ],
                        ];
                        @endphp
                        {{ \TransWidget::renderFields($translationsFieldsConfig) }}

                    </div>
                </div>
                <!--end::Section-->

                <div class="kt-section">
                    <div class="kt-section__content">
                        <div class="form-group row">
                            <div class="col-3">
                                @if(!empty($category['image']))
                                    <img
                                        src="{{ \App\Utilities\ValidationRules\FilePathManager::getFile($category['image'], \App\Constant\ProductFileConstant::TYPE_CATEGORY_IMAGE) }}"
                                        style="width: 350px; height: 310px;" class="rounded">
                                @else
                                    <img
                                        src="{{ asset('images/placeholder-furniture.png') }}"
                                        style="width: 350px; height: 310px;" class="rounded">
                                @endif
                            </div>
                            <div class="col-4">
                                <label for="example-text-input" class="mt-20 mr-20">Изображение за категорията:</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input name="image" type="file" class="custom-file-input" id="image">
                                        <label class="custom-file-label text-left" for="image">
                                            <i class="fa fa-file"></i>
                                        </label>
                                    </div>
                                    <div class="invalid-feedback">
                                        @error('image'){{ $message }}@enderror
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-4">
                        </div>
                        <div class="col-8">
                            <input type="submit" class="btn btn-success" value="Запазване" />
                            <button type="reset" class="btn btn-secondary go-back-btn">Обратно</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection