@php $uniqueIndexForTranslationPanel = \Str::random(5) @endphp
<ul class="nav nav-tabs" role="tablist">
    @foreach($locales as $locale)
        <li class="nav-item">
            <a class="nav-link {{ $loop->first ? 'active' : '' }}" data-toggle="tab" href="#kt_tabs_input_{{ $locale . '_' . $uniqueIndexForTranslationPanel}}">
                <img class="flag-icon" src="{{ asset('images/flags/64x64/' . $locale . '.png') }}" /> {{ $locale }}
            </a>
        </li>
    @endforeach
</ul>
<div class="tab-content">
    @foreach($locales as $locale)
        <div class="tab-pane {{ $loop->first ? 'active' : '' }}" id="kt_tabs_input_{{ $locale . '_' . $uniqueIndexForTranslationPanel }}" role="tabpanel">
            @foreach($fields as $fieldName => $fieldAttributes)
                @php
                    if (isset($fieldAttributes['collection_index_name'])) {
                        $composedFieldName = $fieldAttributes['collection_index_name'] . $fieldsPrefix . $fieldName . ':' . $locale;
                    } else {
                        $composedFieldName = $fieldsPrefix . $fieldName . ':' . $locale;
                    }
                @endphp
                <div class="form-group row">
                    @if (isset($fieldAttributes['type']) && $fieldAttributes['type'] == 'switch_on_off')
                        <label class="col-2 col-form-label text-left">{{ $fieldAttributes['label'] }}</label>
                        <div class="col-10">
                                <span class="kt-switch kt-switch--outline kt-switch--icon">
                                <label>
                                    <input type="hidden" name="{{ $fieldsPrefix . $fieldName }}:{{ $locale }}" value="0">
                                    <input type="checkbox" name="{{ $fieldsPrefix . $fieldName }}:{{ $locale }}" {{ old($fieldsPrefix . $fieldName .':' . $locale, $fieldAttributes['object']->translate($locale)->$fieldName ?? '') ? 'checked="checked"' : '' }}>
                                    <span></span>
                                </label>
                            </span>
                        </div>
                    @else
                        <label for="example-text-input" class="{{ isset($fieldAttributes['type']) && in_array($fieldAttributes['type'], ['rich_text_editor', 'small_rich_text_editor']) ? 'col-12' : 'col-2' }} col-form-label text-left">{{ $fieldAttributes['label'] }}</label>
                        <div class="{{ isset($fieldAttributes['type']) && in_array($fieldAttributes['type'], ['rich_text_editor', 'small_rich_text_editor']) ? 'col-12' : 'col-10' }}">
                            @if (isset($fieldAttributes['type']) && $fieldAttributes['type'] == 'checkbox')
                                <input type="hidden" name="{{ $fieldsPrefix . $fieldName }}:{{ $locale }}" value="0">
                                <input name="{{ $fieldsPrefix . $fieldName }}:{{ $locale }}" type="checkbox" class="form-control align-left" value="1" {{ old($fieldsPrefix . $fieldName .':' . $locale, $fieldAttributes['object']->translate($locale)->$fieldName ?? '') ? 'checked' : '' }} />
                            @elseif (isset($fieldAttributes['type']) && $fieldAttributes['type'] == 'date')
                                <input type="text" name="{{ $fieldsPrefix . $fieldName }}:{{ $locale }}" value="@if(!empty($fieldAttributes['object']->translate($locale))){{$fieldAttributes['object']->translate($locale)->value_by_locale}}@endif" class="form-control kt_datepicker_mysql" readonly="">
                            @elseif (isset($fieldAttributes['type']) && $fieldAttributes['type'] == 'datetime')
                                <input type="text" name="{{ $fieldsPrefix . $fieldName }}:{{ $locale }}" value="@if(!empty($fieldAttributes['object']->translate($locale))){{$fieldAttributes['object']->translate($locale)->value_by_locale}}@endif" class="form-control kt_datetimepicker_mysql" readonly="">
                            @elseif (isset($fieldAttributes['type']) && in_array($fieldAttributes['type'], ['multiple_checkbox']))
                                @foreach($fieldAttributes['choices'] as $choiceValue => $choiceLabel)
                                    <div class="form-check">
                                        <input name="{{ $fieldsPrefix . $fieldName }}:{{ $locale }}[]" type="checkbox" class="form-check-input"
                                               id="{{ $choiceValue }}"  value="{{ $choiceValue ?? '' }}" {{ in_array($choiceValue, old($fieldsPrefix . $fieldName . ':' . $locale, [])) ? 'checked' : (strpos($fieldAttributes['object']->translate($locale)->$fieldName ?? '', $choiceValue) !== false ? 'checked' : '') }}>
                                        <label class="form-check-label" for="{{ $choiceValue ?? '' }}">{{ $choiceLabel }}</label>
                                    </div>
                                @endforeach
                            @elseif (isset($fieldAttributes['type']) && in_array($fieldAttributes['type'], ['rich_text_editor', 'small_rich_text_editor']))
                                <div class="{{ $fieldAttributes['type'] }}-wrapper">
                                     <textarea name="{{ $fieldsPrefix . $fieldName }}:{{ $locale }}" class="{{ $fieldAttributes['type'] }}">
                                     @if (isset($fieldAttributes['object']))
                                         {{ old($fieldsPrefix . $fieldName .':' . $locale, $fieldAttributes['object']->translate($locale)->$fieldName ?? '') }}
                                     @endif

                                </textarea>
                                </div>

                            @else
                                <div class="input-group">
                                    @if (isset($fieldAttributes['object']))
                                        <input name="{{ $fieldsPrefix . $fieldName }}:{{ $locale }}" type="text" value="{{ $fieldAttributes['value'] ?? old($composedFieldName, $fieldAttributes['object']->translate($locale)->$fieldName ?? '') }}" class="form-control"
                                            {{ isset($fieldAttributes['disabled']) ? 'disabled="disabled"' : '' }}
                                        />
                                    @else
                                        <input name="{{ $fieldsPrefix . $fieldName }}:{{ $locale }}" type="text" value="{{ old($fieldsPrefix . $fieldName .':' . $locale) }}" class="form-control"
                                            {{ isset($fieldAttributes['disabled']) ? 'disabled="disabled"' : '' }}
                                        />
                                    @endif

                                </div>
                            @endif
                            @if (isset($fieldAttributes['help']))
                                <span class="form-text text-muted input-hint">{{ $fieldAttributes['help'] }}</span>
                            @endif

                            @error($composedFieldName)
                            <div id="email-error" class="error invalid-feedback" style="display: block;">{{ $message }}</div>
                            @enderror
                        </div>
                    @endif
                </div>
            @endforeach
        </div>
    @endforeach
</div>
