@extends('front.layout')

@section('content')
    <div class="intro py-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-4" style="position: relative;">
                    <a href="#">
                        <img src="themes/scandal/css/images/2020/women-new.png" height="740" width="950" alt="">
                    </a>

                    <a href="#" class="btn btn-bg-white hvr-sweep-to-right">ЖЕНИ</a>
                </div><!-- /.col -->

                <div class="col-sm-4">
                    <a href="#">
                        <img src="themes/scandal/css/images/2020/men-new.png" height="740" width="950" alt="">
                    </a>

                    <a href="#" class="btn btn-bg-white hvr-sweep-to-right">МЪЖЕ</a>
                </div><!-- /.col -->

                <div class="col-sm-4">
                    <a href="#">
                        <img src="themes/scandal/css/images/2020/kids.png" height="740" width="950" alt="">
                    </a>

                    <a href="#" class="btn btn-bg-white hvr-sweep-to-right">ДЕЦА</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div><!-- /.intro -->

    <div class="py-5 section-bestsellers">
        <div class="section-head">
            <div class="section-title h1 mb-3"><i>Bestsellers</i></div><!-- /.section-title -->
        </div><!-- /.section-head -->

        <div class="section-body">
            <div class="container">
                <div class="slider slider-5-items">
                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart active">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/T-9800-red-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">T-SHIRT DAMSKI "FD"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 67,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/T-9800-red-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/SC-000540-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000541-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000542-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000543-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">ZESTAW DRESY DAMSKIE "SC COLLECTION"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 146,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/SC-000540-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000541-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000542-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000543-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/scandal-C0180-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0181-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0182-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0183-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">SPORTOWE DRESY DAMSKIE "SC CLASSIC"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 151,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/scandal-C0180-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0181-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0182-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0183-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/SC-CT070-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT071-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT072-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">ZESTAW DRESY DAMSKIE</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">
                                        <div class="card-old-price">&euro; 174,00</div>
                                        <div class="card-promo-price">&euro; 111,00</div>
                                    </div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/SC-CT070-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT071-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT072-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/T-9300-white-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">T-SHIRT DAMSKI "KIER"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 64,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/T-9300-white-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="kids"><div class="bg-success text-center p-5">3</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">3</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">4</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">5</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">6</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">7</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">9</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">11</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">12</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">13</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">15</div></div>
                </div><!-- /.slider-5-items -->
            </div>
        </div><!-- /.section-body -->
    </div><!-- /.section-bestsellers -->

    <div class="my-4 my-md-5">
        <a href="#">
            <img src="themes/scandal/css/images/2020/scandal-sale.png" height="478" width="1890" alt="">
        </a>
    </div><!-- /.container-fluid -->

    <div class="py-5 section-bestsellers">
        <div class="section-head">
            <div class="section-title h1 mb-3"><i>SCANDAL SALE</i></div><!-- /.section-title -->
        </div><!-- /.section-head -->

        <div class="section-body">
            <div class="container">
                <div class="slider slider-5-items">
                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart active">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/T-9800-red-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">T-SHIRT DAMSKI "FD"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 67,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/T-9800-red-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/SC-000540-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000541-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000542-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000543-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">ZESTAW DRESY DAMSKIE "SC COLLECTION"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 146,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/SC-000540-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000541-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000542-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000543-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/scandal-C0180-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0181-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0182-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0183-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">SPORTOWE DRESY DAMSKIE "SC CLASSIC"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 151,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/scandal-C0180-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0181-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0182-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0183-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/SC-CT070-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT071-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT072-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">ZESTAW DRESY DAMSKIE</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">
                                        <div class="card-old-price">&euro; 174,00</div>
                                        <div class="card-promo-price">&euro; 111,00</div>
                                    </div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/SC-CT070-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT071-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT072-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/T-9300-white-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">T-SHIRT DAMSKI "KIER"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 64,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/T-9300-white-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="kids"><div class="bg-success text-center p-5">3</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">3</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">4</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">5</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">6</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">7</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">9</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">11</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">12</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">13</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">15</div></div>
                </div><!-- /.slider-5-items -->
            </div>
        </div><!-- /.section-body -->
    </div><!-- /.section-bestsellers -->

    <div class="my-4 my-md-5">
        <a href="#">
            <img src="themes/scandal/css/images/2020/new-collection.png" height="478" width="1890" alt="">
        </a>
    </div><!-- /.container-fluid -->

    <div class="py-5 section-bestsellers">
        <div class="section-head">
            <div class="section-title h1 mb-3"><i>НОВО</i></div><!-- /.section-title -->
        </div><!-- /.section-head -->

        <div class="section-body">
            <div class="container">
                <div class="slider slider-5-items">
                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart active">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/T-9800-red-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">T-SHIRT DAMSKI "FD"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 67,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/T-9800-red-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9800-red (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/SC-000540-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000541-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000542-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000543-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">ZESTAW DRESY DAMSKIE "SC COLLECTION"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 146,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/SC-000540-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000541-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000542-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-000543-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/scandal-C0180-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0181-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0182-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0183-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">SPORTOWE DRESY DAMSKIE "SC CLASSIC"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 151,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/scandal-C0180-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0181-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0182-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/scandal-C0183-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/SC-CT070-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT071-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT072-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">ZESTAW DRESY DAMSKIE</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">
                                        <div class="card-old-price">&euro; 174,00</div>
                                        <div class="card-promo-price">&euro; 111,00</div>
                                    </div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/SC-CT070-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT071-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/SC-CT072-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="women">
                        <div class="card card-teaser">
                            <span class="badge badge-danger text-white">-30 %</span>

                            <button type="button" class="btn card-heart">
                                <i class="fal fa-heart"></i>
                            </button>

                            <a href="#" class="card-img-top">
                                <img src="themes/scandal/css/images/temp/T-9300-white-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </a>

                            <div class="card-body">
                                <h6 class="card-title"><a href="#">T-SHIRT DAMSKI "KIER"</a></h6>

                                <div class="card-body-inner">
                                    <div class="card-price">&euro; 64,00</div>

                                    <ul class="card-list">
                                        <li>s</li>

                                        <li>m</li>

                                        <li>l</li>

                                        <li>xl</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-thumbs">
                                <img src="themes/scandal/css/images/temp/T-9300-white-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (2)-1050x1050.jpg" height="1050" width="1050" alt="">

                                <img src="themes/scandal/css/images/temp/T-9300-white (3)-1050x1050.jpg" height="1050" width="1050" alt="">
                            </div><!-- /.card-thumbs -->
                        </div>
                    </div>

                    <div class="kids"><div class="bg-success text-center p-5">3</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">3</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">4</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">5</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">6</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">7</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">9</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">11</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">12</div></div>

                    <div class="kids"><div class="bg-success text-center p-5">13</div></div>

                    <div class="men"><div class="bg-primary text-center p-5">15</div></div>
                </div><!-- /.slider-5-items -->
            </div>
        </div><!-- /.section-body -->
    </div><!-- /.section-bestsellers -->
@endsection