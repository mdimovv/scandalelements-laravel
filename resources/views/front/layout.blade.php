<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Scandal</title>

	<link rel="shortcut icon" type="image/x-icon" href="themes/scandal/css/images/favicon.ico" />

	<!-- Vendor Styles -->
	<link rel="stylesheet" href="themes/scandal/vendor/jquery-ui-1.12.1.custom/jquery-ui.min.css" />
	<link rel="stylesheet" href="themes/scandal/vendor/bootstrap-4.2.1/dist/css/bootstrap.css" />
	<link rel="stylesheet" href="themes/scandal/vendor/fontawesome-pro-5.6.3-web/css/all.min.css" />
	<link rel="stylesheet" href="themes/scandal/vendor/slick-1.8.1/slick/slick.css" />
	<link rel="stylesheet" href="themes/scandal/vendor/slick-1.8.1/slick/slick-theme.css" />
	<link rel="stylesheet" href="themes/scandal/vendor/fancybox-master/dist/jquery.fancybox.min.css" />

	<!-- App Styles -->
	<link rel="stylesheet" href="themes/scandal/css/style.css" />

	<!-- Vendor JS -->
	<script src="themes/scandal/vendor/jquery-1.11.3.min.js"></script>
	<script src="themes/scandal/vendor/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="themes/scandal/vendor/bootstrap-4.2.1/dist/js/bootstrap.bundle.min.js"></script>
	<script src="themes/scandal/vendor/slick-1.8.1/slick/slick.min.js"></script>
	<script src="themes/scandal/vendor/sticky-kit-master/dist/sticky-kit.min.js"></script>
	<script src="themes/scandal/vendor/jquery.ui.touch-punch.min.js"></script>
	<script src="themes/scandal/vendor/fancybox-master/dist/jquery.fancybox.min.js"></script>

	<!-- App JS -->
	<script src="themes/scandal/js/functions.js"></script>
</head>
<body>
<div class="wrapper">
<header class="header">
	<div class="container">
		<nav class="navbar navbar-expand-lg">
			<a class="navbar-brand" href="#">
				<img src="themes/scandal/css/images/2020/logo.png" alt="">
			</a>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mx-auto">

					<li class="nav-item active">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown-women" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ЖЕНИ</a>

						<div class="dropdown-menu" aria-labelledby="navbarDropdown-women">
							<a class="dropdown-item" href="#">
								<img src="themes/scandal/css/images/nav-categories/nav-jackets-vest.jpg" height="215" width="215" alt="">

								<p>Jackets And Vest</p>
							</a>
						</div>
					</li>
				</ul>
			</div>

			<ul class="nav my-lg-0">
				<li class="nav-item">
					<div class="dropdown dropdown-search">
						<a class="nav-link" href="#" id="dropdownMenuButton-search" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fal fa-fw fa-search"></i>
						</a>

						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton-search">
							<form method="post" action="?" class="form-inline">
								<input class="form-control form-control-sm" type="text" placeholder="Search" aria-label="Search">

								<!-- !!! Този бутон ако не събмитва формата, ще трябва да го пренапишем -->
								<button type="submit" class="btn">
									<i class="fal fa-search" aria-hidden="true"></i>
								</button>
							</form>
						</div>
					</div>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="#">
						<i class="fal fa-user"></i>
					</a>
				</li>

				<li class="nav-item">
					<a class="nav-link nav-link-fav" href="#">
						<i class="fal fa-heart"></i>

						<span class="counter">2</span>
					</a>
				</li>
			</ul>

			<div class="dropdown dropdown-cart-wrapper">
				<a href="#" class="nav-link dropdown-toggle btn btn-danger btn-shopping-cart" id="dropdownMenuButton-cart" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fal fa-shopping-bag"></i>

					<div class="btn-shopping-cart-inner"><div>shopping bag (2)</div> <strong>&euro; 100.00</strong></div>
				</a>

				<div class="dropdown-menu dropdown-menu-right dropdown-cart-menu shadow" aria-labelledby="dropdownMenuButton-cart">
					<div class="card">
						<div class="row no-gutters">
							<div class="col-4">
								<a href="#">
									<img src="themes/scandal/css/images/temp/T-9800-red-1050x1050.jpg" height="1050" width="1050" class="card-img" alt="...">
								</a>
							</div>

							<div class="col-8">
								<div class="card-body">
									<button type="button" class="close" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>

									<h6 class="card-title text-uppercase">
										<a href="#">T-shirt Women's "cc"</a>
									</h6>

									<p class="card-text mt-auto mb-2">Size: M</p>

									<p class="card-text">x 1 <span class="price">&euro; 25,00</span></p>
								</div>
							</div>
						</div>
					</div>

					<div class="dropdown-divider my-3"></div>

					<div class="card">
						<div class="row no-gutters">
							<div class="col-4">
								<a href="#">
									<img src="themes/scandal/css/images/temp/T-9800-red-1050x1050.jpg" height="1050" width="1050" class="card-img" alt="...">
								</a>
							</div>

							<div class="col-8">
								<div class="card-body">
									<button type="button" class="close" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>

									<h6 class="card-title text-uppercase">
										<a href="#">T-shirt Women's "cc" T-shirt Women's "cc"</a>
									</h6>

									<p class="card-text mt-auto mb-2">Size: M</p>

									<p class="card-text">x 1 <span class="price">&euro; 75,00</span></p>
								</div>
							</div>
						</div>
					</div>

					<div class="bg-light mt-3 p-3">
						<div class="row align-items-enda">
							<div class="col">TOTAL:</div><!-- /.col -->

							<div class="col"><span class="price">&euro; 100,00</span></div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.bg-light -->

					<div class="container mt-3">
						<div class="row">
							<div class="col pr-2">
								<a href="#" class="btn btn-block btn-dark text-uppercase">View cart</a>
							</div><!-- /.col -->

							<div class="col pl-2">
								<a href="#" class="btn btn-block btn-danger text-uppercase">Checkout</a>
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.container -->
				</div>
			</div>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<i class="fal fa-fw fa-bars"></i>
			</button>
		</nav>
	</div><!-- /.container -->
</header><!-- /.header -->

	@can('manage_system')
		<div class="btn btn-primary btn-admin" style="position: fixed; top: 20%; right: 0; padding: 0.25rem 2rem;">Admin</div>
	@endcan

<main class="main">
    @yield('content')
</main>

<div class="subscribe">
	<div class="container">
		<form action="?" method="post" class="subscribe-form">
			<div class="input-group input-group-lg">
				<div class="input-group-prepend">
					<label for="mail" class="subscribe-label input-group-text text-white"><i>Абонирайте се за нашия бюлетин!</i></label>
				</div>

				<input type="email" id="mail" name="mail" value="" placeholder="Your email" class="form-control subscribe-field" aria-label="Your email">

				<div class="input-group-append">
					<button type="submit" class="btn btn-primary subscribe-btn">
						<span class="far fa-long-arrow-right"></span>
					</button>
				</div>
			</div>
		</form>
	</div><!-- /.container -->
</div><!-- /.subscribe -->

<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-3">
				<p class="footer-col-title">Information</p><!-- /.footer-col-title -->

				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link active" href="#">Contact</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">Faq</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">Return Of Goods</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">Reclamation</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">Privacy Policy</a>
					</li>
				</ul>
			</div><!-- /.col-6 -->

			<div class="col-6 col-md-3">
				<p class="footer-col-title">Delivery</p><!-- /.footer-col-title -->

				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" href="#">Replacement And Returns</a>
					</li><!-- /.nav-item -->

					<li class="nav-item">
						<a class="nav-link" href="#">Delivery And Payment</a>
					</li><!-- /.nav-item -->

					<li class="nav-item">
						<a class="nav-link" href="#">Gdpr Tools</a>
					</li><!-- /.nav-item -->
				</ul>
			</div><!-- /.col-6 -->

			<div class="col-6 col-md-3 mt-4 mt-md-0">
				<p class="footer-col-title">Payment</p><!-- /.footer-col-title -->

				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" href="#">Pay Online</a>
					</li><!-- /.nav-item -->

					<li class="nav-item">
						<a class="nav-link" href="#">On Delivery</a>
					</li><!-- /.nav-item -->
				</ul>

				<p class="footer-col-title mt-4">Contact Us</p><!-- /.footer-col-title -->

				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" href="tel:+48223077909">+48223077909</a>
					</li><!-- /.nav-item -->

					<li class="nav-item">
						<a class="nav-link" href="mailto:info@scandalshop.pl">info@scandalshop.pl</a>
					</li><!-- /.nav-item -->
				</ul>
			</div><!-- /.col-6 -->

			<div class="col-6 col-md-3 mt-4 mt-md-0">
				<p class="footer-col-title">Socials</p><!-- /.footer-col-title -->

				<ul class="nav nav-socials">
					<li class="nav-item">
						<a class="nav-link" href="#">
							<i class="fab fa-facebook-square"></i>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">
							<i class="fab fa-twitter-square"></i>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">
							<i class="fab fa-google-plus-square"></i>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">
							<i class="fab fa-pinterest-square"></i>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">
							<i class="fab fa-skype"></i>
						</a>
					</li>
				</ul>

				<hr>

				<ul class="list-group-payments">
					<i class="sprite-visa-electron"></i>

					<i class="sprite-maestro"></i>

					<i class="sprite-mastercard"></i>

					<i class="sprite-paypal"></i>
				</ul>
			</div><!-- /.col-6 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</footer><!-- /.footer -->

<div class="footer-bar">
	<div class="container">
		<p class="copyrights">&copy; SCANDAL 2019 Всички права запазени.</p><!-- /.copyrights -->

		<p class="credits">Уеб дизайн и поддръжка: <a href="https://rocket.bg/"><i class="sprite-rs"></i></a></p><!-- /.credits -->
	</div><!-- /.container -->
</div><!-- /.footer-bar -->
</div><!-- /.wrapper -->
</body>
</html>
