<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Product extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = [
      'title', 'slug', 'description', 'price', 'promo_price'
    ];

    public $guarded = [];

    public function categories() {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    public function hasCategoryById($categoryId)
    {
        return $this->categories->contains('id', $categoryId);
    }

    public function productSize() {
        return $this->belongsToMany(ProductSize::class, 'product_size');
    }
}
