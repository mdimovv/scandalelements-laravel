<?php


namespace App\Utilities\ValidationRules;


use App\Constant\ProductFileConstant;

class FilePathManager
{
     public static function getFile($fileName, $forTypeConstant = ProductFileConstant::TYPE_GALLERY_IMAGE)
    {
//        if ($forTypeConstant == ProductFileConstant::TYPE_STORE) {
//            $relativePath = env('UPLOAD_STORES_DIRECTORY');
//        } elseif($forTypeConstant == ProductFileConstant::TYPE_CATEGORY_ICON){
//            $relativePath = '/uploads/category/icons/'; }

         if($forTypeConstant == ProductFileConstant::TYPE_CATEGORY_IMAGE){
            $relativePath = '/uploads/category/images/';
         } else {
            $relativePath = env('UPLOAD_IMAGES_DIRECTORY');
        }

        $uploadDirectory = $relativePath;

        $filePath = '';
        if (\File::exists(public_path($relativePath . $fileName))) {
            $filePath = $relativePath. $fileName;
        } else {
            //TO DO
            $filePath = null;
        }

        return $filePath;
    }
}