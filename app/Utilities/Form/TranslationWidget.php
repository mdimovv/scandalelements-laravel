<?php
namespace App\Utilities\Form;

use Astrotomic\Translatable\Locales;


class TranslationWidget
{
    public static function renderFields($fieldsConfig = [], $fieldsPrefix = '')
    {
        $locales = app(Locales::class)->all();

        return view('admin.components.form_widgets.translation_fields', [
            'locales' => $locales,
            'fields' => $fieldsConfig,
            'fieldsPrefix' => $fieldsPrefix ?  $fieldsPrefix . '_' : ''
        ]);
    }
}