<?php

namespace App\Http\Controllers\Admin;

use App\Constant\CacheTagConstant;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Utilities\TreeBuilder;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $categories = Category::orderBy('weight', 'asc')->get()->toArray();

        $categoriesTree = TreeBuilder::buildTree($categories);

        return view('admin.categories.categories_list', [
            'categoriesTree' => $categoriesTree
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        if ($request->get('id')) {
            $category = Category::find($request->get('id'));
        } else {
            $category = new Category();
        }

        if ($request->method() == 'POST') {
            $rules = [
                '%name%' => 'required_with:%description%,%is_enabled%,%meta_description%|string',
                '%description%' => 'nullable|string',
            ];

            $rules = RuleFactory::make($rules);
            $validator = \Validator::make(array_filter($request->all()), $rules);
            if ($validator->fails()) {
                $request->session()->flash('error', 'Записът не може да бъде осъществен !');
                return back()->withErrors($validator)->withInput($request->all());
            } else {
                $cleanData = $request->except([
                    'image'
                ]);

                $category->fill(array_filter($cleanData));
                $category->save();

                $imageFile = $request->file('image');
                if ($imageFile) {
                    $fileName = $imageFile->getClientOriginalName();
                    $destinationPath = public_path() . '/uploads/category/images/';
                    $imageFile->move($destinationPath, $fileName);

                    $category->image = $fileName;
                    $category->save();
                }

//                $category->filterAttributes()->sync($request->input('filterAttributes'));

//                \Cache::tags(CacheTagConstant::CATEGORIES)->flush();

                $request->session()->flash('success', 'Записът беше осъществен успешно !');

                return redirect()->route('categories.edit', ['id' => $category->id]);
            }
        }

        return view('admin.categories.category_edit', [
            'category' => $category,
        ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function saveCategoryTree(Request $request) {
        $categoriesById = Category::all()->keyBy('id');
        $categoryData = $request->get('category_tree');
        $this->updateCategoryTree(null, $categoryData, $categoriesById);

        \Cache::tags(CacheTagConstant::CATEGORIES)->flush();

        return ['status' => 'success'];
    }

    /**
     * @param $parent
     * @param $categoryDataTree
     * @param $categoriesById
     */
    private function updateCategoryTree($parent, $categoryDataTree, &$categoriesById)
    {
        $weight = 0;
        foreach ($categoryDataTree as $category) {
            $categoryId = $category['id'];
            $currentCategoryObj = $categoriesById[$categoryId];
            $currentCategoryObj->weight = $weight;
            $weight += 1;
            // change parent if needed
            if ($parent) {
                if ($currentCategoryObj->parent_id != $parent['id']) {
                    $currentCategoryObj->parent_id = $parent['id'];
                }
            } else {
                if (!empty($currentCategoryObj->parent_id)) {
                    $currentCategoryObj->parent_id = null;
                }
            }
            $currentCategoryObj->save();

            if (isset($category['children']) && count($category['children'])) {
                $this->updateCategoryTree($category, $category['children'], $categoriesById);
            }
        }
    }
}
