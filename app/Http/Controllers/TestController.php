<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function __invoke(Request $request)
    {
        $this->authorize('manage_system');

//        $products = Product::whereHas('productcategory', function ($query) use ($categorySlug) {
//                $query->where('slug', $categorySlug);
//        })->get();

        $products = Product::with('product_category')->whereHas('category_translations', function ($query) {
            $query->where('slug', request()->category);
        })->get();

//        $categories = Category::first();
//        dd($categories->translate('en')->name);
    }
}
