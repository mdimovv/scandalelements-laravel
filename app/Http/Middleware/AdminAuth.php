<?php

namespace App\Http\Middleware;

use Closure;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if (auth()->user() && auth()->user()->can('manage_system')) {
            return $next($request);
        }
        elseif(auth()->user()) {
            abort(403,'Потребител '.auth()->user()->email.' няма достъп до администраторския панел.');
        }
        else {
            return redirect(route('login'), 302);
        }
    }
}
