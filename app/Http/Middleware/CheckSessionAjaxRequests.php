<?php

namespace App\Http\Middleware;

use Closure;

class CheckSessionAjaxRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
          if($request->ajax()) {
            if(empty(\Auth::user())){
                return response()->json([
                    'SESSION_STATUS' => 'NOT_LOGGED_IN'
                ]);
            }
            else{
                return $next($request);
            }
        }
        else{
            return $next($request);
        }
    }
}
