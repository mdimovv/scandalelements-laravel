<?php


namespace App\Constant;


class ProductFileConstant
{
    const TYPE_GALLERY_IMAGE = 'gallery_image'; // just regular image
    const TYPE_CATEGORY_IMAGE = 'category_image';
}