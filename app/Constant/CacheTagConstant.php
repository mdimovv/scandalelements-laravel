<?php
namespace App\Constant;


class CacheTagConstant
{
    const CATEGORIES = "categories"; //use Cache::tags(CacheTagConstant::PRODUCT_CATEGORIES)->flush(); to clear cache when change product categories

    const ALL = [
        self::CATEGORIES,
    ];
}