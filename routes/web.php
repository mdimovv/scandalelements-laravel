<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/

Route::get('/', function () {
    return view('front.homepage');
});

Auth::routes();

Route::get('/account', 'AccountController@index')->name('account');

//Admin
Route::prefix('admin')->group(function(){
    Route::group(['middleware' => ['admin.auth', 'admin.ajax.session_check']], function() {
        Route::any('/test', 'TestController')->name('test');

        Route::get('/', 'Admin\CategoryController@index')->name('categories.list');
        Route::get('/categories/list', 'Admin\CategoryController@index')->name('categories.list');
        Route::post('/categories/save-tree', 'Admin\CategoryController@saveCategoryTree')->name('admin.categories.save');
        Route::any('/categories/edit', 'Admin\CategoryController@edit')->name('categories.edit');

        Route::get('/products/list', 'Admin\ProductController@index')->name('products.list');
    });
});

