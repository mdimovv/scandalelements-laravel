<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'weight' => '0',
            'image' => 'men.jpg',
        ]);

        DB::table('categories')->insert([
            'weight' => '0',
            'image' => 'women.jpg',
        ]);

        DB::table('categories')->insert([
            'weight' => '0',
            'image' => 'kids.jpg',
        ]);

        DB::table('categories')->insert([
            'parent_id' => 1,
            'weight' => '0',
            'image' => 'kids.jpg',
        ]);
    }
}
