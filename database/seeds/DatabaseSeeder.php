<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(RolesTableSeeder::class);
         $this->call(RoleUserTableSeeder::class);
         $this->call(PermissionsTableSeeder::class);
         $this->call(PermissionRoleTableSeeder::class);
         $this->call(CategoriesTableSeeder::class);
         $this->call(CategoryTranslationsTableSeeder::class);
         $this->call(ProductsTableSeeder::class);
         $this->call(ProductTranslationsTableSeeder::class);
         $this->call(ProductSizeTableSeeder::class);
         $this->call(ProductFilesTableSeeder::class);
         $this->call(ProductCategorySeeder::class);
    }
}
