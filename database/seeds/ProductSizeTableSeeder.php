<?php

use Illuminate\Database\Seeder;

class ProductSizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_size')->insert([
            'product_id' => 1,
            'size' => 's',
            'sku' => 'prod1111-s',
            'quantity' => 10
        ]);

        DB::table('product_size')->insert([
            'product_id' => 1,
            'size' => 'm',
            'sku' => 'prod1111-m',
            'quantity' => 10
        ]);

        DB::table('product_size')->insert([
            'product_id' => 2,
            'size' => 'l',
            'sku' => 'prod1111-l',
            'quantity' => 10
        ]);

        DB::table('product_size')->insert([
            'product_id' => 2,
            'size' => 'xl',
            'sku' => 'prod1111-xl',
            'quantity' => 10
        ]);
    }
}
