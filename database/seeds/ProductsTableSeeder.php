<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'sku' => 'prod1111',
            'image' => 'prod_image',
            'color' => 'green',
        ]);

        DB::table('products')->insert([
            'sku' => 'prod222',
            'image' => 'prod_image',
            'color' => 'blue',
        ]);

        DB::table('products')->insert([
            'sku' => 'prod3333',
            'image' => 'prod_image',
            'color' => 'red',
        ]);

        DB::table('products')->insert([
            'sku' => 'prod4444',
            'image' => 'prod_image',
            'color' => 'yellow',
        ]);
    }
}
