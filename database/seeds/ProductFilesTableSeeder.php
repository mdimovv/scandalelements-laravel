<?php

use Illuminate\Database\Seeder;

class ProductFilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_files')->insert([
            'product_id' => 1,
            'thumb_image' => 'image1',
        ]);

        DB::table('product_files')->insert([
            'product_id' => 1,
            'thumb_image' => 'image2',
        ]);

        DB::table('product_files')->insert([
            'product_id' => 1,
            'thumb_image' => 'image3',
        ]);

        DB::table('product_files')->insert([
            'product_id' => 2,
            'thumb_image' => 'image1',
        ]);

        DB::table('product_files')->insert([
            'product_id' => 2,
            'thumb_image' => 'image2',
        ]);

        DB::table('product_files')->insert([
            'product_id' => 2,
            'thumb_image' => 'image3',
        ]);
    }
}
