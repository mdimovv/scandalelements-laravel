<?php

use Illuminate\Database\Seeder;

class CategoryTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_translations')->insert([
            'category_id' => 1,
            'locale' => 'bg',
            'name' => 'Мъже',
            'slug' => 'muje',
        ]);

        DB::table('category_translations')->insert([
            'category_id' => 1,
            'locale' => 'en',
            'name' => 'Men',
            'slug' => 'men',
        ]);

        DB::table('category_translations')->insert([
            'category_id' => 2,
            'locale' => 'bg',
            'name' => 'Жени',
            'slug' => 'jeni',
        ]);

        DB::table('category_translations')->insert([
            'category_id' => 2,
            'locale' => 'en',
            'name' => 'Women',
            'slug' => 'women',
        ]);

        DB::table('category_translations')->insert([
            'category_id' => 3,
            'locale' => 'bg',
            'name' => 'Деца',
            'slug' => 'detsa',
        ]);

        DB::table('category_translations')->insert([
            'category_id' => 3,
            'locale' => 'en',
            'name' => 'Kids',
            'slug' => 'kids',
        ]);

        DB::table('category_translations')->insert([
            'category_id' => 4,
            'locale' => 'bg',
            'name' => 'Подкатегория1',
            'slug' => 'подкатегория1',
        ]);

        DB::table('category_translations')->insert([
            'category_id' => 4,
            'locale' => 'en',
            'name' => 'SubCategory1',
            'slug' => 'subcategory1',
        ]);
    }
}
