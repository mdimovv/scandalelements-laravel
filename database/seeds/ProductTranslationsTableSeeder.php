<?php

use Illuminate\Database\Seeder;

class ProductTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_translations')->insert([
            'product_id' => 1,
            'locale' => 'bg',
            'title' => 'Product1111',
            'slug' => 'product1111',
            'description' => 'this is product 1111',
            'price' => 243,
            'promo_price' => 61
        ]);

        DB::table('product_translations')->insert([
            'product_id' => 1,
            'locale' => 'en',
            'title' => 'Product1111',
            'slug' => 'product1111',
            'description' => 'this is product 1111',
            'price' => 243,
            'promo_price' => 21
        ]);

        DB::table('product_translations')->insert([
            'product_id' => 2,
            'locale' => 'bg',
            'title' => 'Product2222',
            'slug' => 'product2222',
            'description' => 'this is product 2222',
            'price' => 222,
            'promo_price' => 48
        ]);

        DB::table('product_translations')->insert([
            'product_id' => 2,
            'locale' => 'en',
            'title' => 'Product2222',
            'slug' => 'product2222',
            'description' => 'this is product 2222',
            'price' => 222,
            'promo_price' => 111
        ]);
    }
}
