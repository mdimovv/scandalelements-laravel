;(function($, window, document, undefined) {
	var $win = $(window);
	var $doc = $(document);

	$doc.ready(function() {
		//
		// Slider 5 items
		//
		$('.slider-5-items').slick({
			slidesToShow: 5,
  			slidesToScroll: 5,
  			arrows: false,
  			dots: true,
  			responsive: [
	  			{
	  				breakpoint: 1200,
	  				settings: {
	  					slidesToShow: 4,
	  					slidesToScroll: 4,
	  				}
	  			},
	  			{
	  				breakpoint: 992,
	  				settings: {
	  					slidesToShow: 3,
	  					slidesToScroll: 3,
	  				}
	  			},
	  			{
	  				breakpoint: 768,
	  				settings: {
	  					slidesToShow: 2,
	  					slidesToScroll: 2,
	  				}
	  			}
	  		]
		});

		// Slider 5 items buttons functionality
		$('.section-bestsellers .section-head .btn').on('click', function(event) {
			$(this).addClass('active').siblings().removeClass('active');
			$('.slider-5-items').slick('slickUnfilter');
		});

		$('.js-filter-men').on('click', function(){
			$('.slider-5-items').slick('slickFilter','.men');
		});

		$('.js-filter-women').on('click', function(){
			$('.slider-5-items').slick('slickFilter','.women');
		});

		$('.js-filter-kids').on('click', function(){
			$('.slider-5-items').slick('slickFilter','.kids');
		});


		//
		// Sliders - Card teaser
		//
		$('.card-teaser').each(function(index, el) {
			var card = $(this);

			card.find('.card-img-top').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				draggable: false,
				swipe: false,
				asNavFor: card.find('.card-thumbs')
			});

			card.find('.card-thumbs').slick({
				slidesToShow: 6,
				arrows: false,
				vertical: true,
	  			focusOnSelect: true,
				asNavFor: card.find('.card-img-top')
			});
		});


		//
		// Init Tooltip functionality
		//
		$('[data-toggle="tooltip"]').tooltip()


		//
		// Custom jquery ui select
		//
		$.widget( 'app.selectmenu', $.ui.selectmenu, {
			_drawButton: function() {
				this._super();
				var selected = this.element
				.find( '[selected]' )
				.length,
				placeholder = this.options.placeholder;

				if (!selected && placeholder) {
					this.buttonItem.text(placeholder);
				}
			}
		});

		$('.js-custom-select').each(function(index, el) {
			var selectPlaceholder = $(this).data('placeholder');
			
			$(this).selectmenu({
			    placeholder: selectPlaceholder,
			    classes: {
			    	"ui-selectmenu-button": "ui-selectmenu-button-outline",
			    	"ui-selectmenu-menu": "ui-selectmenu-menu-outline"
			    }
			});
		});


		//
		// Sidebar
		//
		// Custom Slider Range Jquery UI
		$( "#slider-range" ).slider({
	    	range: true,
	    	animate: true,
	    	min: 0,
	    	max: 500,
	    	values: [ 75, 300 ],
	    	create: function( event, ui ) {
	    		// console.log($(this).values[0]);
	    		$( "#slider-range" ).find('.ui-slider-handle').first().attr('data-price', $( "#slider-range" ).slider( "values", 0 ) + "лв.");
	    		$( "#slider-range" ).find('.ui-slider-handle').last().attr('data-price', $( "#slider-range" ).slider( "values", 1 ) + "лв.");
	    	},
	    	slide: function( event, ui ) {
	    		$(this).find('.ui-slider-handle').first().attr('data-price', ui.values[ 0 ] + "лв.");
	    		$(this).find('.ui-slider-handle').last().attr('data-price', ui.values[ 1 ] + "лв.");

	    		$( "#price-min" ).val($( "#slider-range" ).slider( "values", 0 ) + "лв.");
     			$( "#price-max" ).val($( "#slider-range" ).slider( "values", 1 ) + "лв.");
	      	}
	    });

		// Set initial value
	    $( "#price-min" ).val($( "#slider-range" ).slider( "values", 0 ) + "лв.");
     	$( "#price-max" ).val($( "#slider-range" ).slider( "values", 1 ) + "лв.");

		// Init Stick in parrent
		$(".sidebar").stick_in_parent();
		
		// Recalc position of fixed sidebar on sidebar callapse toggling
		$('.sidebar .collapse').on('shown.bs.collapse', function () {
			$(".sidebar").trigger("sticky_kit:recalc");
		})

		$('.sidebar .collapse').on('hidden.bs.collapse', function () {
			$(".sidebar").trigger("sticky_kit:recalc");
		})

		// Sidebar section collapse
		$('#sidebar-collapse').on('show.bs.collapse', function (e) {
			if ($(this).is(e.target)) {
				$('html').addClass('noscroll');
			}
		});

		$('#sidebar-collapse').on('hide.bs.collapse', function (e) {
			if ($(this).is(e.target)) {
				$('html').removeClass('noscroll');
			}
		});


		//
		// Product
		//
		// Product sliders
		$('.product-images').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			fade: true,
			arrows: false,
			draggable: false,
			swipe: false,
			asNavFor: $('.product-thumbs'),
			adaptiveHeight: true
		});

		$('.product-thumbs:not(.product-thumbs-horizontal)').slick({
			slidesToShow: 4,
			arrows: false,
			vertical: true,
  			focusOnSelect: true,
			asNavFor: $('.product-images')
		});

		$('.product-thumbs-horizontal').slick({
			slidesToShow: 5,
			arrows: false,
  			focusOnSelect: true,
			asNavFor: $('.product-images')
		});

		// Product large image init gallery
		$('[data-fancybox="gallery"]').fancybox();
	});
})(jQuery, window, document);
